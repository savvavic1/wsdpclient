<!DOCTYPE html>
<?php  include '../controller/VyhledatParceluController.php'; ?>
<html>
<head>
    <title>Vyhledat parcelu</title>
    <style>
        table, tr, th, td {
            border: 1px solid black;
            text-align: center;
        }
    </style>
</head>
<body style="text-align: center">
<h1>Vyhledat parcelu</h1>
<form action="../controller/VyhledatParceluController.php" method="post">
    <label for="kodUzemi"><b>Zadejte kod/nazev KU:</label>
    <input autocomplete="off" list="seznamUzemi" id="kodUzemi" name="kodUzemi"/><br>
    <datalist id="seznamUzemi">
        <?php
        /** @var array $res2 uzemi array */
        foreach ($res2 as $ku) {
            echo '<option value="' . $ku->kod . '">' . $ku->kod .  ';' . $ku->nazev .  ';' . $ku->kodObce . '
                </option>';
        }
        ?>
    </datalist><br>
    <button type="submit">Vybrat</button><br><br>
</form>
</body>
</html>