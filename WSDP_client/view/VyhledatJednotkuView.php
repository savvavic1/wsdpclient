<!DOCTYPE html>
<?php  include '../controller/VyhledatStavbuController.php'; ?>
<html>
<head>
    <title>Vyhledat jednotku</title>
    <style>
        table, tr, th, td {
            border: 1px solid black;
            text-align: center;
        }
    </style>
</head>
<body style="text-align: center">
<h1>Vyhledat jednotku</h1>
<form action="../controller/VyhledatStavbuController.php" method="post">
    <label for="kodObce"><b>Zadejte kod/nazev obce:</label>
    <input autocomplete="off" list="seznamObci" id="kodObce" name="kodObce"/><br>
    <input type="checkbox" name="jednotkaAno" checked="true"><label>Jednotka</label><br>
    <datalist id="seznamObci">
        <?php
        /** @var array $res1 obec array */
        foreach ($res1 as $obec) {
            echo '<option value="' . $obec->kod . '">' . $obec->kod .  ';' . $obec->nazev .  ';' . $obec->okresKod . '
                </option>';
        }
        ?>
    </datalist><br>
    <button type="submit">Vybrat</button>
</form>
</body>
</html>