<?php
/**
 * Customized Web Security Service SOAP header used for user authentication in each SOAP request.
 * @author Victoria Savvateeva
 */

class WsseAuthHeader extends SoapHeader {

    //WSS namespace.
    private $wss_ns = 'http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd';

    /**
     * Create header.
     * @param null $ns namespace
     * @param $user string username (login)
     * @param $pass string password of type Password:Text
     */
    function __construct($user, $pass, $ns = null) {
        //if ($test == false) {
            if ($ns) {
                $this->wss_ns = $ns;
            }
            $auth = new stdClass();
            $auth->Username = new SoapVar($user, XSD_STRING, NULL,
                $this->wss_ns, NULL, $this->wss_ns);
            $auth->Password = new SoapVar($pass, XSD_STRING, NULL,
                $this->wss_ns, NULL, $this->wss_ns);

            $username_token = new stdClass();
            $username_token->UsernameToken = new SoapVar($auth, SOAP_ENC_OBJECT,
                NULL, $this->wss_ns, 'UsernameToken', $this->wss_ns);

            $security_sv = new SoapVar(
                new SoapVar($username_token, SOAP_ENC_OBJECT, NULL,
                    $this->wss_ns, 'UsernameToken', $this->wss_ns),
                SOAP_ENC_OBJECT, NULL, $this->wss_ns, 'Security',
                $this->wss_ns);
            parent::__construct($this->wss_ns, 'Security', $security_sv,
                true);
    }
}