<?php
/**
 * "Sestavy" controller.
 * @author Victoria Savvateeva
 */

include_once '../auth/Client.php';
include_once '../service/SestavyService.php';
include_once '../service/CiselnikService.php';
include_once '../service/InformaceService.php';
include_once '../service/TestServerService.php';

$ss = new SestavyService();
$cs = new CiselnikService();
$cs->callSeznam("KU"); $res = $cs->getSeznamUzemi();

if(isset($_POST['katastrUzemiKod1']) && isset($_POST['lvCislo1']) &&
    isset($_POST['formatPozadavku1'])) {
    if (!isset($_POST['kteryServer'])) echo 'Nezadal(a) jste server!';
    else if ($_POST['kteryServer'] == 'testovaciServer') {
        $ts = new TestServerService();
        if(isset($_POST['datumK1'])) {
        $ts->callVrat($_POST['katastrUzemiKod1'], $_POST['lvCislo1'], $_POST['datumK1'],
            $_POST['formatPozadavku1']);
        }
        else $ts->callVrat($_POST['katastrUzemiKod1'], $_POST['lvCislo1'], null,
            $_POST['formatPozadavku1']);
    }
    else if ($_POST['kteryServer'] == 'serverKatastru') {
        if(isset($_POST['datumK1'])) {
            $ss->callGenerujLVZjednodusene($_POST['katastrUzemiKod1'], $_POST['lvCislo1'],
                $_POST['formatPozadavku1'], $_POST['datumK1']);
        }
        else $ss->callGenerujLVZjednodusene($_POST['katastrUzemiKod1'], $_POST['lvCislo1'],
            $_POST['formatPozadavku1']);
    }
}

if(isset($_POST['katastrUzemiKod2']) && isset($_POST['lvCislo2']) && isset($_POST['formatPozadavku2'])) {
    if ($_POST['katastrUzemiKod2'] != '' && $_POST['lvCislo2'] != '' &&
        $_POST['formatPozadavku2'] != '') {
        $ss->callGenerujLVZjednodusene($_POST['katastrUzemiKod2'], $_POST['lvCislo2'],
            $_POST['formatPozadavku2']);
    }
}

if(isset($_POST['generovatMBR'])) {
    $i = 0; $array = [];
    foreach($_POST['check_list'] as $selected) {
        $array[$i++] = $selected;
    }
    $is = new InformaceService();
    $is->callDejMBRParcel($array);
}

if(isset($_POST['idSestavy'])) {
    if (isset($_POST['formatHotovy']))  $ss->callVratSestavu($_POST['idSestavy'], $_POST['formatHotovy']);
    else $ss->callVratSestavu($_POST['idSestavy']);
}

if(isset($_POST['osobaId'])) {
    if (isset($_POST['format']))  $ss->callGenerujPrehledVlastnictvi($_POST['osobaId'], $_POST['format']);
    else $ss->callVratSestavu($_POST['osobaId']);
}

if(isset($_POST['idListiny'])) {
    $ss->callGenerujVystupZeSbirkyListin($_POST['idListiny']);
}

if(isset($_POST['datumOd']) && isset($_POST['formatVypisu'])) {
    $ss->callVypisUctu($_POST['datumOd'], $_POST['formatVypisu']);
}