<?php

include_once '../view/CiselnikVypisView.html';
include_once '../auth/Client.php';

class CiselnikService
{
    private $client;

    private $uzemiArr;
    private $obceArr;
    private $castiObciArr;
    private $druhyPozemkuArr;
    private $zpusobOchranyArr;
    private $vyuzitiPozemkuArr;
    private $vyuzitiJednotkyArr;
    private $vyuzitiStavbyArr;
    private $typJednotkyArr;

    private $res;

    public function __construct()
    {
        //$this->client = new Client("http://localhost:80/WSDP_cacheServer_LH/Server.php?wsdl");
        $this->client = new Client("https://katastr.cuzk.cz:443/ws/wsdp/2.8/ciselnik?wsdl");
    }

    public function callSeznam($ceho, $kodCastiObce = null)
    {
        $obj = "seznam" . $ceho;
        try {
            if (is_null($kodCastiObce) == 0) {
                $request_param = array('kde');
                $request_param['kde'] = array('kodObce');
                $request_param['kde']['kodObce'] = array('odpovida' => $kodCastiObce);
                $this->res = $this->client->getSoapClient()->$obj($request_param);

            }
            else {
                $this->res = $this->client->getSoapClient()->$obj();
                //var_dump($this->res);
                //$arr = get_object_vars($this->res);
                //echo '<pre>';
                //print_r($arr);
                //echo '</pre>';
            }
            if (gettype($this->res->vysledek->zprava) != 'array') {
                if ($this->res->vysledek->zprava->kod != 0) {
                    echo '<br>Zadal(a) jste neplatne udaje.<br>';
                    return;
                }
            } else if ($this->res->vysledek->zprava[1]->kod != 0) {
                echo '<br>Zadal(a) jste neplatne udaje.<br>';
                return;
            }

            if ($ceho == 'Obci') $this->obceArr = $this->res->obec;
            else if ($ceho == 'KU') $this->uzemiArr = $this->res->katastralniUzemi;
            else if ($ceho == 'CastiObci') $this->castiObciArr = $this->res->castObce;
            else if ($ceho == 'DruhuPozemku') $this->druhyPozemkuArr = $this->res->druhPozemku;
            else if ($ceho == 'ZpusobOchrany') $this->zpusobOchranyArr = $this->res->zpusobOchrany;
            else if ($ceho == 'VyuzitiPozemku') $this->vyuzitiPozemkuArr = $this->res->vyuzitiPozemku;
            else if ($ceho == 'VyuzitiJednotky') $this->vyuzitiJednotkyArr = $this->res->vyuzitiJednotky;
            else if ($ceho == 'VyuzitiStavby') $this->vyuzitiStavbyArr = $this->res->vyuzitiStavby;
            else if ($ceho == 'TypuJednotek') $this->typJednotkyArr = $this->res->typJednotky;

        } catch (SoapFault $e) {
            echo '<br>Zadal(a) jste neplatne udaje. catch<br>';
            echo $this->client->getSoapClient()->__getLastRequest();
            echo $e->getMessage();
            return;
        }
    }

    public function getSeznamUzemi()
    {
        return $this->uzemiArr;
    }

    public function getSeznamDruhuPozemku()
    {
        return $this->druhyPozemkuArr;
    }

    public function getSeznamZpusobOchrany()
    {
        return $this->zpusobOchranyArr;
    }

    public function getSeznamZpusobVyuzitiPozemku()
    {
        return $this->vyuzitiPozemkuArr;
    }

    public function getSeznamZpusobVyuzitiJednotky()
    {
        return $this->vyuzitiJednotkyArr;
    }

    public function getSeznamZpusobVyuzitiStavby()
    {
        return $this->vyuzitiStavbyArr;
    }

    public function getSeznamObci()
    {
        return $this->obceArr;
    }

    public function getSeznamCastiObci()
    {
        return $this->castiObciArr;
    }

    public function getSeznamTypuJednotek()
    {
        return $this->typJednotkyArr;
    }

    public function printSeznamDruhuPozemku($arr)
    {
        echo '<h2>DRUHY POZEMKU</h2>';
        echo '<table style="width: 100%">
  <tr>
    <th>Kod</th>
    <th>Nazev</th></tr>';
        foreach ($arr as $druh) {
            echo '<tr><td>' . $druh->kod . '</td>
<td>' . $druh->nazev . '</td></tr>';
        }
    }

    public function printSeznamTypuJednotek($arr)
    {
        echo '<h2>TYPY JEDNOTEK</h2>';
        echo '<table style="width: 100%">
  <tr>
    <th>Kod</th>
    <th>Nazev</th></tr>';
        foreach ($arr as $typ) {
            echo '<tr><td>' . $typ->kod . '</td>
<td>' . $typ->nazev . '</td></tr>';
        }
    }

    public function printSeznamZpusobOchrany($arr)
    {
        echo '<h2>ZPUSOBY OCHRANY POZEMKU</h2>';
        echo '<table style="width: 100%">
  <tr>
    <th>Kod</th>
    <th>Nazev</th></tr>';
        foreach ($arr as $zpOch) {
            echo '<tr><td>' . $zpOch->kod . '</td>
<td>' . $zpOch->nazev . '</td></tr>';
        }
    }

    public function printSeznamZpusobVyuzitiPozemku($arr)
    {
        echo '<h2>ZPUSOBY VYUZITI POZEMKU</h2>';
        echo '<table style="width: 100%">
  <tr>
    <th>Kod</th>
    <th>Nazev</th></tr>';
        foreach ($arr as $zpVyuz) {
            echo '<tr><td>' . $zpVyuz->kod . '</td>
<td>' . $zpVyuz->nazev . '</td></tr>';
        }
    }

    public function printSeznamZpusobVyuzitiJednotky($arr)
    {
        echo '<h2>ZPUSOBY VYUZITI JEDNOTKY</h2>';
        echo '<table style="width: 100%">
  <tr>
    <th>Kod</th>
    <th>Nazev</th></tr>';
        foreach ($arr as $zpVyuz) {
            echo '<tr><td>' . $zpVyuz->kod . '</td>
<td>' . $zpVyuz->nazev . '</td></tr>';
        }
    }

    public function printSeznamZpusobVyuzitiStavby($arr)
    {
        echo '<h2>ZPUSOBY VYUZITI STAVBY</h2>';
        echo '<table style="width: 100%">
  <tr>
    <th>Kod</th>
    <th>Nazev</th></tr>';
        foreach ($arr as $zpVyuz) {
            echo '<tr><td>' . $zpVyuz->kod . '</td>
<td>' . $zpVyuz->nazev . '</td></tr>';
        }
    }

    public function printSeznamKU($arr)
    {
        echo '<h2>KATASTRALNI UZEMI</h2>';
        echo '<table style="width: 100%">
  <tr>
    <th>Kod</th>
    <th>Nazev</th> 
    <th>Kod obce</th></tr>';
        foreach ($arr as $uzemi) {
            echo '<tr><td>' . $uzemi->kod . '</td>
<td>' . $uzemi->nazev . '</td><td>' . $uzemi->kodObce . '</td></tr>';
        }
    }
}