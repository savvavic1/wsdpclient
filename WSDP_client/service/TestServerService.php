<?php

/**
 * "Server" web service consumer.
 * @author Victoria Savvateeva
 */

include_once '../auth/Client.php';

class TestServerService
{
    private $client;

    public function __construct()
    {
        $this->client = new Client("https://dev.pointkn.tnspraha1.cz:443/WSDP_cacheServer_REM/Server.php?wsdl", true);
    }

    public function callVrat($kod, $lv, $datum, $format)
    {
        $filename = $kod . '_' . $lv . '.' . $format;
        $request_param['filename'] = $filename;
        try {
            $res = $this->client->getSoapClient()->vrat($request_param);
            //echo htmlentities($this->client->getSoapClient()->__getLastRequest());
            //echo htmlentities($this->client->getSoapClient()->__getLastResponse());
            //$arr = get_object_vars($res);
            //echo '<pre>';
            //print_r($arr);
            //echo '</pre>';
            if ($res->result != null) {
                file_put_contents($lv . "FromTestServer." . $format,
                    base64_decode($res->result));
                echo 'Success.<br>';
            }
            else echo 'Soubor nenalezen.<br>';
        }
        catch (SoapFault $e) {
            //echo htmlentities($this->client->getSoapClient()->__getLastRequest());
            echo $e->getMessage();
        }
    }
}